/*
 * measureTask.h
 *
 *  Created on: Feb 10, 2017
 *      Author: thongthai
 */

#ifndef SRC_MEASURETASK_H_
#define SRC_MEASURETASK_H_

#include "FreeRTOS.h"
#include "task.h"

void vMeasureStart( uint16_t usStackSize, UBaseType_t uxPriority );

#endif /* SRC_MEASURETASK_H_ */
