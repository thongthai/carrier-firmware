#ifndef _XMODEM_H_
#define _XMODEM_H_

#include <ff.h>

int xmodemReceive(unsigned char *dest, int destsz);
int xmodemTransmitFile(FIL *file);

#endif /* _XMODEM_H_ */
