#include "captureTask.h"

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xparameters.h>
#include <xaxidma.h>
#include "PDa_ADC_IPCore.h"
#include "serial_adc.h"

static void prvCaptureTask( void *pvParameters );

FATFS fs;
static char capture_started = 0;

static XAxiDma dma;

#define BUFFER_SIZE sizeof(unsigned int) * 16
#define PDA_ADC_BASEADDRESS 0x43c10000
volatile static unsigned int data_buffer[16];
static unsigned int *bufferPtr;


void initCaptureDMA()
{
	XAxiDma_Config *Config;
	int status;
	Config = XAxiDma_LookupConfig(XPAR_AXI_DMA_0_DEVICE_ID);

	status = XAxiDma_CfgInitialize(&dma, Config);

	// Reset DMA
	XAxiDma_Reset(&dma);
	while (!XAxiDma_ResetIsDone(&dma));

	//disable interrupts, we'll be polling
	XAxiDma_IntrDisable(&dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(&dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);
}





void initCapture()
{
	// printf won't work properly because the CLI task hasn't started and
	// initialized the serial port yet at this point

	FRESULT res;
	int status;

	initCaptureDMA();

	bufferPtr = data_buffer; //(unsigned int *)BUFFER_ADDRESS;

	//status = PDA_ADC_IPCORE_Reg_SelfTest(PDA_ADC_BASEADDRESS);
	//PDA_ADC_IPCORE_mWriteReg(PDA_ADC_BASEADDRESS, PDA_ADC_IPCORE_S04_AXI_SLV_REG4_OFFSET, 0x0);

	//mount the SD card's filesystem
	res = f_mount(&fs, path, 1);
			;
	if (res != FR_OK) {
		//printf( "Error mounting SD card\r\n" );
		return pdFALSE;
	}
	//printf("Mounted SD card\r\n");


}

void startCapture()
{
	capture_started = 1;
}
void stopCapture()
{
	XAxiDma_Reset(&dma);

	capture_started = 0;
}

void vCaptureStart( uint16_t usStackSize, UBaseType_t uxPriority )
{
	/* Create that task that handles the console itself. */
	xTaskCreate( 	prvCaptureTask,	/* The task that implements the command console. */
					"CAPTURE",						/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
					usStackSize,				/* The size of the stack allocated to the task. */
					NULL,						/* The parameter is not used, so NULL is passed. */
					uxPriority,					/* The priority allocated to the task. */
					NULL );						/* A handle is not required, so just pass NULL. */
}
/*-----------------------------------------------------------*/

static void prvCaptureTask( void *pvParameters )
{

	BaseType_t xReturned;

	FRESULT res;
	FIL fil;

	const TickType_t xIntervalDelay = 10 / portTICK_PERIOD_MS;
	const char number_of_captures_per_file = 5;

	( void ) pvParameters;

	int i = 0;
	char *filename = malloc(15); //"capture.dat";

	SERIAL_ADC_mWriteReg(XPAR_SERIAL_ADC_0_S00_AXI_BASEADDR, SERIAL_ADC_S00_AXI_SLV_REG0_OFFSET, 0x00);

	static char str[50];

	for( ;; )
	{
		if (!capture_started) continue;

		static unsigned int counter = 0;

		sprintf(filename, "cap%d.dat", counter++);

		res = f_open( &fil, filename, FA_WRITE | FA_CREATE_ALWAYS);

		if (res != FR_OK) {
			//printf( "Error opening file '%s'\r\n", filename );
			capture_started = 0;
			continue;
		}

		static unsigned int bytes_written = 0;

		for (i = 0; i < number_of_captures_per_file; i++)
		{
			SERIAL_ADC_mWriteReg(XPAR_SERIAL_ADC_0_S00_AXI_BASEADDR, SERIAL_ADC_S00_AXI_SLV_REG3_OFFSET, 0x01);
			SERIAL_ADC_mWriteReg(XPAR_SERIAL_ADC_0_S00_AXI_BASEADDR, SERIAL_ADC_S00_AXI_SLV_REG3_OFFSET, 0x00);
			vTaskDelay( xIntervalDelay ); //wait for the FIFO to reset

			char *str_ptr;
			int n = 0;

			str_ptr = str;

			//begin each frame of data on a new line
			sprintf(str_ptr, "\n");
			f_write( &fil, str_ptr, 1, &bytes_written);

			//fill the FIFO buffer with one frame of data
			SERIAL_ADC_mWriteReg(XPAR_SERIAL_ADC_0_S00_AXI_BASEADDR, SERIAL_ADC_S00_AXI_SLV_REG0_OFFSET, 0x01);

			while(!SERIAL_ADC_mReadReg(XPAR_SERIAL_ADC_0_S00_AXI_BASEADDR, SERIAL_ADC_S00_AXI_SLV_REG1_OFFSET)){ //capture_fin
				//wait till capture is done
			}

			int count = 0;

			//grab one frame of data
			while(!SERIAL_ADC_mReadReg(XPAR_SERIAL_ADC_0_S00_AXI_BASEADDR, SERIAL_ADC_S00_AXI_SLV_REG2_OFFSET)) { //Stop-while from hardware
				int result;
				bufferPtr = data_buffer;

				count++;

				str_ptr = str;

				// Reset DMA
				XAxiDma_Reset(&dma);
				while (!XAxiDma_ResetIsDone(&dma));
				Xil_DCacheDisable();

				result = XAxiDma_SimpleTransfer(&dma, (UINTPTR) bufferPtr, BUFFER_SIZE, XAXIDMA_DEVICE_TO_DMA);

				//Read_go -- Go for read_state
				SERIAL_ADC_mWriteReg(XPAR_SERIAL_ADC_0_S00_AXI_BASEADDR, SERIAL_ADC_S00_AXI_SLV_REG0_OFFSET, 0x03); //slv_reg1(1) = read_go = 1;

				while ((XAxiDma_Busy(&dma,XAXIDMA_DEVICE_TO_DMA))) {
						/* Wait */
				}

				SERIAL_ADC_mWriteReg(XPAR_SERIAL_ADC_0_S00_AXI_BASEADDR, SERIAL_ADC_S00_AXI_SLV_REG0_OFFSET, 0x02);


				for (int j = 0; j < 8; j++) {
					n = sprintf(str_ptr, "%u,", bufferPtr[j]);
					str_ptr += n;
				}
				Xil_DCacheEnable();

				int str_len = strlen(str);


				f_write( &fil, str, str_len, &bytes_written);

			}

			//vTaskDelay( xIntervalDelay );

		}

		f_close( &fil );
		capture_started = 0;

	}

	vTaskDelete( NULL );

}
