#include "measureTask.h"

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xparameters.h>
#include "Tec_IPCore.h"
#include "xqspips.h"
#include "ads79XX.h"

static XQspiPs qspi;

static void prvMeasureTask( void *pvParameters );

void initSPI()
{
	int status;
	XQspiPs_Config *QspiConfig;

	QspiConfig = XQspiPs_LookupConfig(XPAR_PS7_QSPI_0_DEVICE_ID);

	status = XQspiPs_CfgInitialize(&qspi, QspiConfig, QspiConfig->BaseAddress);
	status = XQspiPs_SelfTest(&qspi);

	XQspiPs_SetClkPrescaler(&qspi, XQSPIPS_CLK_PRESCALE_8);


}

void vMeasureStart( uint16_t usStackSize, UBaseType_t uxPriority )
{
	initSPI();
	/* Create that task that handles the console itself. */
	xTaskCreate( 	prvMeasureTask,	/* The task that implements the command console. */
					"MEASURE",						/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
					usStackSize,				/* The size of the stack allocated to the task. */
					NULL,						/* The parameter is not used, so NULL is passed. */
					uxPriority,					/* The priority allocated to the task. */
					NULL );						/* A handle is not required, so just pass NULL. */
}
/*-----------------------------------------------------------*/

static void prvMeasureTask( void *pvParameters )
{

	BaseType_t xReturned;

	const TickType_t xIntervalDelay = 1000 / portTICK_PERIOD_MS;

	int measured_temperature = 0;
	unsigned short cmd;

	//initialize the ADC
	cmd = ADS79XX_PROG_AUTO1;
	XQspiPs_PolledTransfer(&qspi, &cmd, NULL, 2);

	cmd = AUTO1_CHAN0_ENABLE;
	XQspiPs_PolledTransfer(&qspi, &cmd, NULL, 2);

	// Program Auto Mode 1 Range and configuration settings
	cmd = ADS79XX_AUTO_MODE1 | AUTO1_PROG_ENABLE | AUTO1_CHAN_COUNT_RESET | AUTO1_5VRANGE_SEL | AUTO1_NORM_OP_SEL | AUTO1_CHAN_ADDY_SEL;
	XQspiPs_PolledTransfer(&qspi, &cmd, NULL, 2);

	for( ;; )
	{

		char recv_buf[2];
		cmd = 0x0000;
		//get measurements from ADC
		XQspiPs_PolledTransfer(&qspi, &cmd, recv_buf, 2);
		measured_temperature = recv_buf[1] << 1 | recv_buf[0];

		//update TEC controller with new temperature value
		TEC_IPCORE_mWriteReg(XPAR_TEC_IPCORE_0_S03_AXI_BASEADDR, TEC_IPCORE_S03_AXI_SLV_REG1_OFFSET, measured_temperature);

		vTaskDelay( xIntervalDelay );
	}

	vTaskDelete( NULL );

}
