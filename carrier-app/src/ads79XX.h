// Copyright (c) 2005-2011 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This header contains bit field words used to communicate and configure the
// various features of the ADS79XX family of devices

#ifndef ADS79XX_H_
#define ADS79XX_H_

//Special operations
#define ADS79XX_CONT_OP			0x0000			//Device continues operation in previously defined parameters

//Mode Operation Words
#define ADS79XX_MAN_MODE		0x1000			//Continue Operation in Manual Mode
#define ADS79XX_AUTO_MODE1		0x2000			//Continue Operation in Auto Mode 1
#define ADS79XX_AUTO_MODE2		0x3000			//Continue Operation in Auto Mode 2

//Mode Programming Words
#define ADS79XX_PROG_AUTO1		0x8000			//Enter Programming Mode for Auto Mode 1
#define	ADS79XX_PROG_AUTO2		0x9000			//Enter Programming Mode for Auto Mode 2

//Manual Mode Bit Fields
#define MAN_PROG_ENABLE			0x0800			//Enables Programming bits DI06-00
#define MAN_PROG_DISABLE		0x0000			//Disables Programming bits DI06-00
#define MAN_CHANNEL_SEL_0		0x0000			//Selects Channel 0 for next conversion
#define MAN_CHANNEL_SEL_1		0x0080			//Selects Channel 1 for next conversion
#define MAN_CHANNEL_SEL_2		0x0100			//Selects Channel 2 for next conversion
#define MAN_CHANNEL_SEL_3		0x0180			//Selects Channel 3 for next conversion
#define MAN_CHANNEL_SEL_4		0x0200			//Selects Channel 4 for next conversion
#define MAN_CHANNEL_SEL_5		0x0280			//Selects Channel 5 for next conversion
#define MAN_CHANNEL_SEL_6		0x0300			//Selects Channel 6 for next conversion
#define MAN_CHANNEL_SEL_7		0x0380			//Selects Channel 7 for next conversion
#define MAN_CHANNEL_SEL_8		0x0400			//Selects Channel 8 for next conversion
#define MAN_CHANNEL_SEL_9		0x0480			//Selects Channel 9 for next conversion
#define MAN_CHANNEL_SEL_10		0x0500			//Selects Channel 10 for next conversion
#define MAN_CHANNEL_SEL_11		0x0580			//Selects Channel 11 for next conversion
#define MAN_CHANNEL_SEL_12		0x0600			//Selects Channel 12 for next conversion
#define MAN_CHANNEL_SEL_13		0x0680			//Selects Channel 13 for next conversion
#define MAN_CHANNEL_SEL_14		0x0700			//Selects Channel 14 for next conversion
#define MAN_CHANNEL_SEL_15		0x0780			//Selects Channel 15 for next conversion
#define MAN_25VRANGE_SEL		0x0000			//Next conversion uses 2.5V range
#define MAN_5VRANGE_SEL			0x0040			//Next conversion uses 5V range
#define MAN_NORM_OP_SEL			0x0000			//Device continues in normal operation
#define	MAN_POW_DOWN_SEL		0x0020			//Device enters power down on 16th falling edge of SCLK
#define MAN_CHAN_ADDY_SEL		0x0000			//SDO outputs channel address on DO15-DO12 on next frame
#define MAN_GPIO_DATA_SEL		0x0010			//SDO outputs GPIO data on DO15-DO12 on next frame

//Auto Mode 1 Bit Fields
#define AUTO1_PROG_ENABLE		0x0800			//Enables programming bits DI10-DI00
#define AUTO1_PROG_DISABLE		0x0000			//Disables programming bits DI10-DI00
#define AUTO1_CHAN_COUNT_RESET	0x0400			//Resets channel counter to the lowest channel enabled
#define AUTO1_CHAN_COUNT_CONT	0x0000			//Channel counter continues incrementing normally
#define AUTO1_25VRANGE_SEL		0x0000			//Next conversion uses 2.5V range
#define AUTO1_5VRANGE_SEL		0x0040			//Next conversion uses 5V range
#define AUTO1_NORM_OP_SEL		0x0000			//Device continues in normal operation
#define AUTO1_POW_DOWN_SEL		0x0020			//Device enters power down on 16th falling edge of SCLK
#define AUTO1_CHAN_ADDY_SEL		0x0000			//SDO outputs channel address on DO15-DO12 on next frame
#define AUTO1_GPIO_DATA_SEL		0x0010			//SDO outputs GPIO data on DO15-DO12 on next frame

//Auto Mode 1 Programming Mode Bit Fields
//These words should only be used in Frame 2 programming sequence for auto mode 1
#define AUTO1_CHAN0_ENABLE		0x0001			//Enable channel 0 in Auto Mode 1 Sequence
#define AUTO1_CHAN1_ENABLE		0x0002			//Enable channel 1 in Auto Mode 1 Sequence
#define AUTO1_CHAN2_ENABLE		0x0004			//Enable channel 2 in Auto Mode 1 Sequence
#define AUTO1_CHAN3_ENABLE		0x0008			//Enable channel 3 in Auto Mode 1 Sequence
#define AUTO1_CHAN4_ENABLE		0x0010			//Enable channel 4 in Auto Mode 1 Sequence
#define AUTO1_CHAN5_ENABLE		0x0020			//Enable channel 5 in Auto Mode 1 Sequence
#define AUTO1_CHAN6_ENABLE		0x0040			//Enable channel 6 in Auto Mode 1 Sequence
#define AUTO1_CHAN7_ENABLE		0x0080			//Enable channel 7 in Auto Mode 1 Sequence
#define AUTO1_CHAN8_ENABLE		0x0100			//Enable channel 8 in Auto Mode 1 Sequence
#define AUTO1_CHAN9_ENABLE		0x0200			//Enable channel 9 in Auto Mode 1 Sequence
#define AUTO1_CHAN10_ENABLE		0x0400			//Enable channel 10 in Auto Mode 1 Sequence
#define AUTO1_CHAN11_ENABLE		0x0800			//Enable channel 11 in Auto Mode 1 Sequence
#define AUTO1_CHAN12_ENABLE		0x1000			//Enable channel 12 in Auto Mode 1 Sequence
#define AUTO1_CHAN13_ENABLE		0x2000			//Enable channel 13 in Auto Mode 1 Sequence
#define AUTO1_CHAN14_ENABLE		0x4000			//Enable channel 14 in Auto Mode 1 Sequence
#define AUTO1_CHAN15_ENABLE		0x8000			//Enable channel 15 in Auto Mode 1 Sequence
#define AUTO1_ALL_CHAN_ENABLE	0xFFFF			//Enables all channels in Auto Mode 1 Sequence

//Auto Mode 2 Programming Mode Bit Fields
//These words are used in the same frame as the entrance to Auto Mode 2 Programming
#define AUTO2_CHANRNG_SEL_0		0x0000			//Channel range is only channel 0
#define AUTO2_CHANRNG_SEL_1		0x0080			//Channel range is CH0-CH1
#define AUTO2_CHANRNG_SEL_2		0x0100			//Channel range is CH0-CH2
#define AUTO2_CHANRNG_SEL_3		0x0180			//Channel range is CH0-CH3
#define AUTO2_CHANRNG_SEL_4		0x0200			//Channel range is CH0-CH4
#define AUTO2_CHANRNG_SEL_5		0x0280			//Channel range is CH0-CH5
#define AUTO2_CHANRNG_SEL_6		0x0300			//Channel range is CH0-CH6
#define AUTO2_CHANRNG_SEL_7		0x0380			//Channel range is CH0-CH7
#define AUTO2_CHANRNG_SEL_8		0x0400			//Channel range is CH0-CH8
#define AUTO2_CHANRNG_SEL_9		0x0480			//Channel range is CH0-CH9
#define AUTO2_CHANRNG_SEL_10	0x0500			//Channel range is CH0-CH10
#define AUTO2_CHANRNG_SEL_11	0x0580			//Channel range is CH0-CH11
#define AUTO2_CHANRNG_SEL_12	0x0600			//Channel range is CH0-CH12
#define AUTO2_CHANRNG_SEL_13	0x0680			//Channel range is CH0-CH13
#define AUTO2_CHANRNG_SEL_14	0x0700			//Channel range is CH0-CH14
#define AUTO2_CHANRNG_SEL_15	0x0780			//Channel range is CH0-CH15

#endif /*ADS79XX_H_*/
