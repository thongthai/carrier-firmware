/*
 * captureTask.h
 *
 *  Created on: Feb 10, 2017
 *      Author: thongthai
 */

#ifndef SRC_CAPTURETASK_H_
#define SRC_CAPTURETASK_H_

#include "FreeRTOS.h"
#include "task.h"

#include <ff.h>

extern FATFS fs;
static char *path = "0:/";

void initCapture();
void startCapture();
void stopCapture();

void vCaptureStart( uint16_t usStackSize, UBaseType_t uxPriority );

#endif /* SRC_CAPTURETASK_H_ */
